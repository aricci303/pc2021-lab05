package acme;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.Optional;

import cartago.*;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;


public class RoomDigitalTwinArtifact extends Artifact {

	private String uri;
	private HttpClient client;
	private Vertx vertx;
	private String uriWithPort;
	private int port;

	public void init(String uri, int port) throws Exception {
		this.uri = uri;
		this.port = port;
		this.uriWithPort = uri + ":" + port;
		client = HttpClient.newHttpClient();	
		bindToPhysicalThing();
		log("ready.");
	}


	private void bindToPhysicalThing() throws Exception {
		
		log("connecting to " + uri);

		/* read initial state */
		
		JsonObject temp = this.doGetBlocking(uriWithPort + "/properties/temperature");
		defineObsProperty("temperature", temp.getDouble("temperature"));
		
		JsonObject status = this.doGetBlocking(uriWithPort + "/properties/state");
		defineObsProperty("state", status.getString("state"));

		/* subscribe */
		
		vertx = Vertx.vertx();
		RoomDigitalTwinArtifact art = this;
		vertx.createHttpClient().websocket(port, uri, "/", ws -> {
	
			/* handling ws msgs */
			
			ws.handler(msg -> {
				try {
					JsonObject ev = new JsonObject(msg.toString());		    	  
					String msgType = ev.getString("messageType");
					if (msgType.equals("propertyStatus")) {
						JsonObject data = ev.getJsonObject("data");
						Double newTemperature = data.getDouble("temperature");
						String newState = data.getString("state");

						// log("updating artifact state with " + newTemperature + " " + newState);

						art.beginExtSession();							
						if (newTemperature != null) {
							ObsProperty tprop = getObsProperty("temperature");
							tprop.updateValue(newTemperature);
						}
						if (newState != null) {
							ObsProperty sprop = getObsProperty("state");
							sprop.updateValue(newState);
						}
						art.endExtSession();
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			});		
						
			JsonObject msg = new JsonObject();
			msg
			.put("messageType", "addEventSubscription")
			.put("data", new JsonObject());
			
			ws.writeTextMessage(msg.toString());		
		});
	}	

	@OPERATION void getCurrentTemperature(OpFeedbackParam<Double> temp) {
		try {
			log("getting the temperature.");
			JsonObject obj = this.doGetBlocking(uriWithPort + "/properties/temperature");
			temp.set(obj.getDouble("temperature"));
		} catch (Exception ex) {
			failed("");
		}
	}

	@OPERATION void getCurrentState(OpFeedbackParam<String> state) {
		try {
			log("getting the state.");
			JsonObject obj = this.doGetBlocking(uriWithPort + "/properties/state");
			state.set(obj.getString("state"));
		} catch (Exception ex) {
			failed("");
		}
	}

	@OPERATION void startCooling() {
		try {
			log("start cooling.");
			JsonObject msg = new JsonObject();
			msg.put("startCooling", new JsonObject()
					.put("input", new JsonObject()));
			this.doPostBlocking(uriWithPort + "/actions/startCooling",Optional.of(msg));
		} catch (Exception ex) {
			failed("");
		}
	}

	@OPERATION void startHeating() {
		try {
			log("start heating.");
			JsonObject msg = new JsonObject();
			msg.put("startHeating", new JsonObject()
					.put("input", new JsonObject()));
			this.doPostBlocking(uriWithPort + "/actions/startHeating",Optional.of(msg));
		} catch (Exception ex) {
			failed("");
		}
	}

	@OPERATION void stopWorking() {
		try {
			log("start working.");
			JsonObject msg = new JsonObject();
			msg.put("stopWorking", new JsonObject()
					.put("input", new JsonObject()));
			this.doPostBlocking(uriWithPort + "/actions/stopWorking",Optional.of(msg));
		} catch (Exception ex) {
			failed("");
		}
	}


	// aux actions

	private JsonObject doGetBlocking(String uri) throws Exception {
		try {
			var request = HttpRequest.newBuilder()
					.uri(URI.create("http://" + uri))
					.build();		

			var response = client.send(request,  BodyHandlers.ofString());
			return new JsonObject(response.body());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	private JsonObject doPostBlocking(String uri, Optional<JsonObject> body) throws Exception {
		HttpRequest req = null;
		// log("doing a post at " + "http://" + uri);
		if (!body.isEmpty()) {
			req  = HttpRequest.newBuilder()
					.uri(URI.create("http://" + uri))
					.POST(BodyPublishers.ofString(body.get().toString()))
					.build();		

		} else {
			req = HttpRequest.newBuilder()
					.uri(URI.create("http://" + uri))
					.POST(BodyPublishers.noBody())
					.build();		
		}

		var response = client.send(req, BodyHandlers.ofString());
		return new JsonObject(response.body());
	}

}
